HAPPY_BIRTHDAY = [
    """
    t120
    o4v100
    g8.g16 a4g4>c4 <b2g8.g16a4g4>d4c2<g8.g16>g4e4c4<b4a4&a2.>f8.f16e4c4d4c1p2.
    """,
    """
    o4v80
    r8.r16 cee <g>ff <g>ff cee cee <fa&a &a2. g>c<b >e1p2.
    """
]

# Modified from https://archeagemmllibrary.com/beethoven-fur-elise-2/
FUR_ELISE = [
    """t65v90
    o5l16>ed+v120ed+ec-dc<a8rceab8reg+bb+8re>ed+ed+ec-dc<a8rceab8rdb+ba8rb>
    cde8.<g>fed8.<f>edc8.<e>dc<br8r>er8rer8d+er8d+ed+ed+ec-dc<a8rceab8reg+
    bb+8re>ed+ed+ec-dc<a8rceab8rt40db+t20ba8
    """,
    """v90
    o5r2l16<<a>ear8&r16<e>eg+r8&r16<a>ear2r<a>ear8&r16<e>eg+r8&r16<a>ea
    r8&r16cgb+r8&r16<g>gbr8&r16<a>ear8&r16<e>e>eere>eerd+er8d+er2r<<<a>ea
    r8&r16<e>eg+r8&r16<a>ear2r<a>ear8&r16<e>eg+r8&r16<a8
    """
]

